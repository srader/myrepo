#include <stdio.h>
#include <stdlib.h>



// $GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
int main()
{
    char gps[100];
    printf("Enter GPS string: ");
    fgets(gps, 100, stdin);
    gps_decode(*gps);
}

int gps_decode(char *gps)
{
    int quality, nsats, checksum;
    float lat, lon, accuracy, alt, ellipsoid;
    char ns, ew;
    char timestamp[12];
    sscanf(gps, "$GPGGA,%[1234567890.],%f,%c,%f,%c,%d,%d,%f,%f,M,%f,M,,*%x",
                timestamp,
                &lat,
                &ns,
                &lon,
                &ew,
                &quality,
                &nsats,
                &accuracy,
                &alt,
                &ellipsoid,
                &checksum);
    
    printf("Time: %.*s:%.*s:%s\nLat: %dD%fM %c\nLong: %f %c\nAlt: %f\n",
            //timestamp[0], timestamp[1], timestamp[2], timestamp[3], timestamp+4,
            2, timestamp, 2, timestamp+2, timestamp+4,
            (int)(lat / 100), lat - ((int)(lat / 100)) * 100, ns,
            lon, ew,
            alt);
    
    return 1;
}